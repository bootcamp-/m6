from django.urls import path
from .views import indexView, addVehiculo, registroView, loginView, listarVehiculo, logoutView

urlpatterns = [
    path('', indexView, name='index'),
    path('add/', addVehiculo, name='addvehiculo'),
    path('registro/', registroView, name='registro'),
    path('login/', loginView, name='login'),
    path('listar/', listarVehiculo, name='listar'),
    path('logout/', logoutView, name='logout')
]